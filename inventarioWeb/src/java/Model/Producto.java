package Model;

public class Producto {
    private int id_producto;
    private String nom_pruducto;
    private int categoria_id;
    private float stock;
    private float precio;
    private String unidadMedida;
    private int estado;
    private Categoria categoria; //objeto categoria

    public Producto(){
        this.id_producto = 0;
        this.categoria = new Categoria();//inicializar el modelo categoria
    }
    public Producto(int id_producto, String nom_producto, int categoria_id, float stock, float precion, String unidadMedida, int estado , Categoria categoria){
        this.id_producto = id_producto;
        this.nom_pruducto = nom_producto;
        this.categoria_id = categoria_id;
        this.stock = stock;
        this.precio = precion;
        this.unidadMedida = unidadMedida;
        this.estado = estado;
        this.categoria = categoria;
    }
    
    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getNom_pruducto() {
        return nom_pruducto;
    }

    public void setNom_pruducto(String nom_pruducto) {
        this.nom_pruducto = nom_pruducto;
    }

    public int getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }

    public float getStock() {
        return stock;
    }

    public void setStock(float stock) {
        this.stock = stock;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
}
