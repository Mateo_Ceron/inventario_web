package Factory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class ConexionDB {
    protected String[] parametros;  //Array que recibe los parametros de la conexion... nombre BD, usuario, passs
    protected Connection conexion;
    /* El siguiente metodo abstracto no se implemtea solamente se declara, 
    se implementara en la subclase*/
    abstract Connection open(); //METODO ABSTRACTO QUE DEVUELVE UN OBEJTO CONEXION
    
    //Crear netodo para las consultas
    public ResultSet consultaSQL(String consulta){
        Statement st;   //Objeto Statement en el encargado de ejecutar las consultas
        ResultSet rs=null; //tabla temporal donde se almacenan los datos
        try{
            st = conexion.createStatement();
            rs = st.executeQuery(consulta); //Se ejecuta la consulta
        }catch(SQLException ex){
                ex.printStackTrace();
        }
        return rs;
    }
    
    //crear metodo para ejecutar
    public boolean ejecutarSQL(String consulta){
        Statement st;
        boolean guardar = true;
        try{
            st = conexion.createStatement();
            st.executeUpdate(consulta);//se ejecuta la consulta
        }catch(SQLException ex){
            guardar = false;
            ex.printStackTrace();
        }
        return guardar;
    }
    
    //metodo para cerrar la conexion
    public boolean cerrarConexion(){
        boolean ok = true;
            try{
                conexion.close();;
            }catch(Exception ex){
                ok = false;
                ex.printStackTrace();
            }
        return ok;
    }
}
